#pragma once

#include "homeassistant_rest.h"
#include "esphome/core/component.h"
#include "esphome/components/switch/switch.h"

namespace esphome {
namespace homeassistant_rest_ {

class HomeassistantRestSwitch : public switch_::Switch, public PollingComponent {
 public:
  HomeassistantRestSwitch();

  void dump_config() override;
  void update() override;

  void set_ip_address(const char *_ip_address);
  void set_entity_id(const char *_entity_id);
  void set_port(const int _port);
  void set_token(const char *_token);

 protected:
  
  void write_state(bool state) override;
  HomeassistantRest hassApi;
};

}  // namespace homeassistant_rest_
}  // namespace esphome
