#pragma once

#include "homeassistant_rest.h"
#include "homeassistant_rest_light.h"
#include "esphome/core/component.h"
#include "esphome/components/light/light_output.h"
#include "esphome/core/log.h"
#include "esphome/components/api/api_server.h"

namespace esphome {
  namespace homeassistant_rest_ {
    static const char *const TAG = "homeassistant_rest.light";

    HomeassistantRestLightOutput::HomeassistantRestLightOutput() : PollingComponent(1000) {
      
    }


    // overwrites
    void HomeassistantRestLightOutput::dump_config() {
      ESP_LOGCONFIG(TAG, "Homeassistant API LightOutput:");
      ESP_LOGCONFIG(TAG, this->hassApi.getConfigDumpAddress().c_str());
      ESP_LOGCONFIG(TAG, this->hassApi.getConfigDumpURL().c_str());
    }

    light::LightTraits HomeassistantRestLightOutput::get_traits() {
      auto traits = light::LightTraits();
      traits.set_supported_color_modes({light::ColorMode::RGB});
      return traits;
    }
//
    void HomeassistantRestLightOutput::write_state(light::LightState *light) {

      auto values = light->remote_values;
      float _brightness = values.get_brightness()*255;
      float _red = values.get_red();//*255;
      float _green = values.get_green();//*255;
      float _blue = values.get_blue();//*255;
      float _state = values.get_state();
      
      ESP_LOGD(TAG,"State %f, RGB - %f,%f,%f, Bright %f",_state,_red,_green,_blue,_brightness); 
      int returnCode = 0;
      this->hassApi.slowDown(3);
      if(values.is_on()){
        returnCode = this->hassApi.turnLightOn(_brightness,_red,_green,_blue);
      } else {
        returnCode = this->hassApi.turnLightOff();
      }
      if(returnCode != 200) {
        ESP_LOGE(TAG,"Error on update light - %i",returnCode); 
      }
    }
//
    void HomeassistantRestLightOutput::update() {
      
      int returnCode = this->hassApi.polling();
      if(returnCode == 200) {
        // Cast state from remote ESP
        int updateRed=this->red, updateGreen=this->green, updateBlue=this->blue, updateBrightness=this->brightness, updateState=0;
        StaticJsonDocument<1000> lastWebResponse = this->hassApi.lastResponse();

        char* statename = strdup(lastWebResponse["state"]);
        updateState = strcmp(statename, "on");
        
        if(updateState>=0){
          updateState=1;
          updateBrightness = lastWebResponse["attributes"]["brightness"];
          updateRed = lastWebResponse["attributes"]["rgb_color"][0];
          updateGreen = lastWebResponse["attributes"]["rgb_color"][1];
          updateBlue = lastWebResponse["attributes"]["rgb_color"][2];
        } else {
          updateState=0;
        }

        if((this->state != updateState) || (this->brightness != updateBrightness)|| (this->red != updateRed)|| (this->green != updateGreen)|| (this->blue != updateBlue))  {

          ESP_LOGD(TAG,"Polling - update found!"); 
          ESP_LOGD(TAG,"Local: On: %i, R %i,G %i, B %i, Dimm: %i",this->state,this->red,this->green,this->blue,this->brightness);
          ESP_LOGD(TAG,"Remote: On: %i, R %i,G %i, B %i, Dimm: %i",updateState,updateRed,updateGreen,updateBlue,updateBrightness);

          this->state = updateState;
          this->brightness = updateBrightness;
          this->red = updateRed;
          this->green = updateGreen;
          this->blue = updateBlue;

          if(updateState == 1) {
            auto call = this->light_state_->turn_on();
            call.set_brightness(updateBrightness/255.0);
            call.set_red(updateRed/255.0);
            call.set_blue(updateBlue/255.0);
            call.set_green(updateGreen/255.0);
            call.perform();
          } else {  
            auto call = this->light_state_->turn_off();
            call.perform();
          }
          
        }
        
      }   
      
    }

    
    // Parameter Setters
    void HomeassistantRestLightOutput::set_ip_address(const char *_ip_address) {
      this->hassApi.setIpAddress(_ip_address);
    }

    void HomeassistantRestLightOutput::set_port(const int _port) {
      this->hassApi.setPort(_port);
    }

    void HomeassistantRestLightOutput::set_token(const char *_token) {
      this->hassApi.setToken(_token);
    }

    void HomeassistantRestLightOutput::set_entity_id(const char *_entity_id){
      this->hassApi.setEntityId(_entity_id);
    }


  }  // namespace homeassistant_rest_
}  // namespace esphome

