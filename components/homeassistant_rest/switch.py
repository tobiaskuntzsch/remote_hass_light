import esphome.codegen as cg
import esphome.config_validation as cv
from esphome.components import switch
from esphome.const import (
    CONF_IP_ADDRESS,
    CONF_ENTITY_ID,
    CONF_PORT,
)
CONF_TOKEN = "token"

from . import homeassistant_rest_ns
HomeassistantRestSwitch = homeassistant_rest_ns.class_("HomeassistantRestSwitch", switch.Switch, cg.PollingComponent)


CONFIG_SCHEMA = cv.All(
    switch.switch_schema(HomeassistantRestSwitch)
    .extend(
        {
            cv.Required(CONF_TOKEN): cv.string,
            cv.Required(CONF_IP_ADDRESS): cv.string,
            cv.Required(CONF_ENTITY_ID): cv.string,
            cv.Optional(CONF_PORT): cv.int_,
            
        },
    )
    .extend(cv.polling_component_schema("1s"))
)

async def to_code(config):
    var = await switch.new_switch(config)
    cg.add(var.set_token(config[CONF_TOKEN]))
    cg.add(var.set_ip_address(config[CONF_IP_ADDRESS]))
    cg.add(var.set_entity_id(config[CONF_ENTITY_ID]))
    if CONF_PORT in config:
        cg.add(var.set_port(config[CONF_PORT]))
    await cg.register_component(var, config)
    
