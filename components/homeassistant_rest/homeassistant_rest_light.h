#pragma once

#include "homeassistant_rest.h"
#include "esphome/core/component.h"
#include "esphome/components/light/light_output.h"
#include "esphome/core/log.h"

namespace esphome {
namespace homeassistant_rest_ {

class HomeassistantRestLightOutput : public PollingComponent, public light::LightOutput {
  public:
    HomeassistantRestLightOutput();

    light::LightTraits get_traits() override ;
    void dump_config();
    void write_state(light::LightState *light) override;
    void setup_state(light::LightState *state) override { this->light_state_ = state; }

    void update() override;

    void set_ip_address(const char *_ip_address);
    void set_port(const int _port);
    void set_token(const char *_token);
    void set_entity_id(const char *_entity_id);
    

  protected:
    HomeassistantRest hassApi;
    int red=255, green=255, blue=255, brightness=255,state=0;
    light::LightState *light_state_{nullptr};

};

}  // namespace homeassistant_rest_
}  // namespace esphome

