import esphome.codegen as cg

CODEOWNERS = ["@TobiasKuntzsch"]

homeassistant_rest_ns = cg.esphome_ns.namespace("homeassistant_rest_")
