#pragma once

#include <Arduino.h>
#include <ArduinoJson.h>

#ifdef USE_ESP8266
  #include <ESP8266HTTPClient.h>
#else
  #include <HTTPClient.h>
#endif

typedef String TSTRING;
namespace esphome {
    namespace homeassistant_rest_ {

        class HomeassistantRest
        {
            public:
                // Init
                HomeassistantRest();
                
                // RestApi Configuration
                void setIpAddress(String _ip_address);
                void setPort(int _port);

                void setEntityId(String _entity_id);
                void setToken(String _auth_enabbled);
                
                String getConfigDumpAddress();
                String getConfigDumpURL();
                
                StaticJsonDocument<1000> lastResponse();

                // WebClient Configuration
                void setTimeout(int _timeout = 5000);
                void slowDown(int _slow_down = 10);

                // Sync with Webserver API
                int polling();
                int sendPostRequest(String _url,String _post_body);
                
                int turnLightOn();
                int turnLightOn(int brightness, int red, int green, int blue);
                int turnLightOff();

                int turnSwitchOn();
                int turnSwitchOff();

            private:
                String resturl;
                StaticJsonDocument<1000> lastWebResponse;
                String ip_address;
                int port;
                String entity_id;
                String token;
                int timeout;
                int errorSlowDown = 0;
                
        };
    }  // namespace homeassistant_rest_
}  // namespace esphome
