#include "homeassistant_rest_switch.h"
#include "esphome/core/log.h"

namespace esphome {
  namespace homeassistant_rest_ {

    static const char *const TAG = "homeassistant_rest.switch";

    HomeassistantRestSwitch::HomeassistantRestSwitch(){

    };

    // overwrites
    void HomeassistantRestSwitch::dump_config() {
      ESP_LOGCONFIG(TAG, "Homeassistant API Switch:");
      ESP_LOGCONFIG(TAG, this->hassApi.getConfigDumpAddress().c_str());
      ESP_LOGCONFIG(TAG, this->hassApi.getConfigDumpURL().c_str());
    }

    void HomeassistantRestSwitch::write_state(bool state) {
      int returnCode = 0;
      
      if(state) {
        returnCode = this->hassApi.turnSwitchOn();
      } else {
        returnCode = this->hassApi.turnSwitchOff();
      }

      if(returnCode == 200) {
        if(state){
          this->publish_state("on");
        } else {
          this->publish_state("off");
        }
      } 

    }

    void HomeassistantRestSwitch::update() {
      
      int returnCode = this->hassApi.polling();

      if(returnCode == 200) {
        StaticJsonDocument<1000> lastWebResponse = this->hassApi.lastResponse();
        bool publish_state = lastWebResponse["state"]=="on";
        this->publish_state(publish_state);
      } 
      
    }

    // Parameter Setters
    void HomeassistantRestSwitch::set_ip_address(const char *_ip_address) {
      this->hassApi.setIpAddress(_ip_address);
    }

    void HomeassistantRestSwitch::set_port(const int _port) {
      this->hassApi.setPort(_port);
    }

    void HomeassistantRestSwitch::set_token(const char *_token) {
      this->hassApi.setToken(_token);
    }

    void HomeassistantRestSwitch::set_entity_id(const char *_entity_id){
      this->hassApi.setEntityId(_entity_id);
    }

  }  // namespace homeassistant_rest_
}  // namespace esphome
