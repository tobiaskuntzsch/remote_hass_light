import esphome.codegen as cg
import esphome.config_validation as cv
from esphome.components import light, output
from esphome.const import (
    CONF_OUTPUT_ID,
    CONF_IP_ADDRESS,
    CONF_ENTITY_ID,
    CONF_PORT
)

CONF_TOKEN = "token"

from . import homeassistant_rest_ns
HomeassistantRestLightOutput = homeassistant_rest_ns.class_("HomeassistantRestLightOutput", cg.PollingComponent, light.LightOutput)

CONFIG_SCHEMA = cv.All(
    light.RGB_LIGHT_SCHEMA.extend(
        {
            cv.GenerateID(CONF_OUTPUT_ID): cv.declare_id(HomeassistantRestLightOutput),
            cv.Required(CONF_TOKEN): cv.string,
            cv.Required(CONF_IP_ADDRESS): cv.string,
            cv.Required(CONF_ENTITY_ID): cv.string,
            cv.Optional(CONF_PORT): cv.int_,
        }
    )
)


async def to_code(config):
    var = cg.new_Pvariable(config[CONF_OUTPUT_ID])
    cg.add(var.set_token(config[CONF_TOKEN]))
    cg.add(var.set_ip_address(config[CONF_IP_ADDRESS]))
    cg.add(var.set_entity_id(config[CONF_ENTITY_ID]))
    if CONF_PORT in config:
        cg.add(var.set_port(config[CONF_PORT]))
    await cg.register_component(var, config)
    await light.register_light(var, config)
    


