#include "homeassistant_rest.h"
#include "base64.h"
#include "esphome/core/log.h"

namespace esphome {
  namespace homeassistant_rest_ {

        static const char *const TAG = "homeassistant_rest.core";

        HomeassistantRest::HomeassistantRest(){
            this->timeout=5000;
            this->ip_address="127.0.0.1";
            this->port=8123;
            this->token="dummy";
            this->entity_id="dummy";
        }


        void HomeassistantRest::setIpAddress(String _ip_address) {
            this->ip_address = _ip_address;
            this->resturl = "http://"+this->ip_address+":"+this->port+"/api";
        }

        void HomeassistantRest::setPort(int _port) {
            this->port = _port;
            this->resturl = "http://"+this->ip_address+":"+this->port+"/api";
        }

        void HomeassistantRest::setToken(String _token) {
            this->token = _token;
        }

        void HomeassistantRest::setEntityId(String _entity_id){
            this->entity_id = _entity_id;
        }

        void HomeassistantRest::setTimeout(int _timeout){
            this->timeout = _timeout;
        }

        void HomeassistantRest::slowDown(int _slow_down){
            this->errorSlowDown = _slow_down;
        }

        String HomeassistantRest::getConfigDumpAddress(){
            return "  IP Address: " + this->ip_address+":"+this->port+" (timeout "+String(this->timeout)+")";
        }
        String HomeassistantRest::getConfigDumpURL(){
            return "  Base URL: " + this->resturl;
        }
        
        int HomeassistantRest::polling(){
            if(this->errorSlowDown == 0) {
                ESP_LOGV(TAG,"Polling homeassistant api!");

                String stateurl = resturl +"/states/"+this->entity_id;
                ESP_LOGV(TAG,"  URL - %s",stateurl.c_str());

                HTTPClient httpPolling;
                httpPolling.begin(stateurl.c_str());
                httpPolling.setConnectTimeout(this->timeout);
                httpPolling.addHeader("Content-Type", "application/json");
                String authHeader = "Bearer " + this->token;
                httpPolling.addHeader("Authorization", authHeader.c_str());
                
                int returnCode = httpPolling.GET();
                
                if(returnCode == 200) {
                    ESP_LOGV(TAG,"Get new state from api (%s:%i) for %s - %i",this->ip_address.c_str(),this->port,this->entity_id.c_str(),returnCode);
                    String responseString = httpPolling.getString();
                    deserializeJson(lastWebResponse, responseString);
                } else {
                    ESP_LOGE(TAG,"Can't connect to api  (%s:%i) for %s - %i",this->ip_address.c_str(),this->port,this->entity_id.c_str(),returnCode);
                    ESP_LOGD(TAG,stateurl.c_str());
                    this->errorSlowDown = 30;
                }
                return returnCode;
            } else {
                this->errorSlowDown = this->errorSlowDown - 1;
                return -99;
            }
        }

        int HomeassistantRest::sendPostRequest(String _url,String _post_body){
            ESP_LOGV(TAG,"Turn Light on homeassistant api (simple)!");
            ESP_LOGV(TAG,"  URL - %s",_url.c_str());

            HTTPClient httpPostRequest;
            httpPostRequest.begin(_url.c_str());
            httpPostRequest.setConnectTimeout(this->timeout);
            httpPostRequest.addHeader("Content-Type", "application/json");
            String authHeader = "Bearer " + this->token;
            httpPostRequest.addHeader("Authorization", authHeader.c_str());

            ESP_LOGV(TAG,"  Body - %s",_post_body.c_str());
            int returnCode = httpPostRequest.POST(_post_body.c_str());
            
            if(returnCode == 200) {
                ESP_LOGV(TAG,"Get new state from api (%s:%i) for %s - %i",this->ip_address.c_str(),this->port,this->entity_id.c_str(),returnCode);
                String responseString = httpPostRequest.getString();
                deserializeJson(lastWebResponse, responseString);
            } else {
                ESP_LOGE(TAG,"Can't connect to api  (%s:%i) for %s - %i",this->ip_address.c_str(),this->port,this->entity_id.c_str(),returnCode);
                ESP_LOGD(TAG,_url.c_str());
                this->errorSlowDown = 30;
            }
            return returnCode;
        }


        int HomeassistantRest::turnLightOn(){
            
            String actionUrl = resturl +"/services/light/turn_on";
            String postBody = "{\"entity_id\": \""+this->entity_id+"\"}";
            
            return sendPostRequest(actionUrl,postBody);

        }

        int HomeassistantRest::turnLightOn(int brightness, int red, int green, int blue){
            
            String actionUrl = resturl +"/services/light/turn_on";
           
            String postBody = "{\"entity_id\": \""+this->entity_id+"\", \"brightness\": "+String(brightness)+",\"rgb_color\": ["+String(red)+", "+String(green)+", "+String(blue)+"]}";
            
            return sendPostRequest(actionUrl,postBody);

        }

        int HomeassistantRest::turnLightOff(){
            
            String actionUrl = resturl +"/services/light/turn_off";
            String postBody = "{\"entity_id\": \""+this->entity_id+"\"}";
            
            return sendPostRequest(actionUrl,postBody);

        }

        int HomeassistantRest::turnSwitchOn(){
            
            String actionUrl = resturl +"/services/switch/turn_on";
            String postBody = "{\"entity_id\": \""+this->entity_id+"\"}";
            
            return sendPostRequest(actionUrl,postBody);

        }

        int HomeassistantRest::turnSwitchOff(){
            
            String actionUrl = resturl +"/services/switch/turn_off";
            String postBody = "{\"entity_id\": \""+this->entity_id+"\"}";
            
            return sendPostRequest(actionUrl,postBody);

        }

        StaticJsonDocument<1000> HomeassistantRest::lastResponse(){
            return lastWebResponse;
        }
    }
}